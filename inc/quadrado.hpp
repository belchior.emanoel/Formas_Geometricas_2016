#ifndef QUADRADO_H
#define QUADRADO_H

#include "formageometrica.hpp"

class Quadrado : public FormaGeometrica {

	public:
		Quadrado();
		Quadrado(float largura);
};
#endif