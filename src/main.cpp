#include <iostream>
#include "quadrado.hpp"
#include "triangulo.hpp"

using namespace std;

int main() {

	Quadrado * forma1 = new Quadrado(20.0);
	Triangulo * forma2 = new Triangulo(20, 30);

	forma1->calculaArea();
	forma1->calculaPerimetro();

	forma2->calculaArea();
	forma2->calculaPerimetro();

	cout << "Área do Quadrado: " << forma1->getArea() << endl;
	cout << "Perímetro do Quadrado: " << forma1->getPerimetro() << endl;

	cout << "Área do Triângulo: " << forma2->getArea() << endl;
	cout << "Perímetro do Triângulo: " << forma2->getPerimetro() << endl;

	delete(forma1);
	delete(forma2);
}





