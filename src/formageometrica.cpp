#include <iostream>
#include "formageometrica.hpp"

using namespace std;

FormaGeometrica::FormaGeometrica() {
	setLados(0);
	setLargura(10);
	setAltura(20);
}

FormaGeometrica::FormaGeometrica(float altura, float largura) {
	setLados(0);
	setLargura(largura);
	setAltura(altura);	
}

void FormaGeometrica::setAltura(float altura) {
	this->altura = altura;
}
void FormaGeometrica::setLargura(float largura) {
	this->largura = largura;
}
void FormaGeometrica::setLados(int lados) {
	this->lados = lados;
}
float FormaGeometrica::getAltura() {
	return altura;
}
float FormaGeometrica::getLargura() {
	return largura;
}
int FormaGeometrica::getLados() {
	return lados;
}

float FormaGeometrica::getArea() {
	return area;
}
float FormaGeometrica::getPerimetro() {
	return perimetro;
}

float FormaGeometrica::calculaArea() {
	this->area = largura * altura;
	return this->area;
}
float FormaGeometrica::calculaPerimetro() {
	this->perimetro = 2*largura + 2*altura;
	return this->perimetro;
}

void FormaGeometrica::setArea(float area) {
	this->area = area;
}
void FormaGeometrica::setPerimetro(float perimetro) {
	this->perimetro = perimetro;
}











